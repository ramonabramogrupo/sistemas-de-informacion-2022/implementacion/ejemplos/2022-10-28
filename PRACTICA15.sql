﻿SET NAMES 'utf8';
USE ciclistas;



-- Ejercicio 1.- 	Nombre y edad de los ciclistas que han ganado etapas y puertos. 
-- Lo debéis realizar con una intersección. 
-- Como MYSQL no dispone de esta clausula lo vais a realizar 
-- primero  utilizando NATURAL JOIN Y después utilizando IN.


  -- c1 : CICLISTAS QUE HAN GANADO ETAPAS
  SELECT DISTINCT e.dorsal FROM etapa e;

  -- c2 : CICLISTAS QUE HAN GANADO PUERTOS
  SELECT DISTINCT p.dorsal FROM puerto p;
     

  -- c3: CICLISTAS QUE HAN GANADO ETAPAS Y PUERTOS
  -- INTERSECCION

  -- CON NATURAL JOIN
  SELECT * FROM 
    (SELECT DISTINCT e.dorsal FROM etapa e) c1 
    NATURAL JOIN 
    (SELECT DISTINCT p.dorsal FROM puerto p) c2; 

  -- CON IN
  SELECT DISTINCT e.dorsal 
    FROM etapa e 
    WHERE e.dorsal IN (SELECT DISTINCT p.dorsal FROM puerto p);

  -- la interseccion realizada con in es esta
  SELECT 
      c1.dorsal 
    FROM 
      ( 
        SELECT DISTINCT e.dorsal FROM etapa e
      ) c1
    WHERE 
      dorsal IN (SELECT DISTINCT p.dorsal FROM puerto p);

  -- la interseccion con un semijoin
  SELECT 
      c1.dorsal 
    FROM 
      (
         SELECT DISTINCT e.dorsal FROM etapa e
      ) c1
    WHERE 
      EXISTS (SELECT 1 FROM puerto p WHERE p.dorsal=c1.dorsal);

  
  -- Consulta final 
  SELECT 
    c.nombre,c.edad 
    FROM (
           SELECT * FROM 
              (SELECT DISTINCT e.dorsal FROM etapa e) c1 
              NATURAL JOIN 
              (SELECT DISTINCT p.dorsal FROM puerto p) c2         
         ) AS c3
    JOIN ciclista c USING(dorsal);


-- Ejercicio 2.- 	Nombre y edad de los ciclistas que no han ganado etapas. 
-- Debéis realizarlo con una resta de conjuntos. 
-- Como MYSQL no dispone de esta cláusula quiero que lo realicéis utilizando 
-- LEFT JOIN y después con NOT IN


-- c1 : CICLISTAS QUE HAN GANADO ETAPAS
SELECT DISTINCT dorsal FROM etapa;


-- c2
-- todos los ciclistas
SELECT DISTINCT c.dorsal FROM ciclista c;


-- c3
-- resta ==> c2-c1


-- implemento la resta con un left join

SELECT 
    c2.dorsal 
  FROM 
      (
        SELECT DISTINCT c.dorsal FROM ciclista c
      ) c2 
    LEFT JOIN
      (
        SELECT DISTINCT dorsal FROM etapa
      ) c1 USING(dorsal)
  WHERE 
    c1.dorsal IS NULL;

-- implemento la resta con un not in

SELECT 
    c2.dorsal 
  FROM 
    ( 
      SELECT DISTINCT c.dorsal FROM ciclista c 
    ) c2
  WHERE 
    c2.dorsal NOT IN (SELECT DISTINCT dorsal FROM etapa);

-- implementar la resta con un antisemijoin (not exists)

SELECT 
    c2.dorsal 
  FROM 
    (
      SELECT DISTINCT c.dorsal FROM ciclista c 
    ) c2
  WHERE 
    NOT EXISTS (SELECT 1 FROM etapa e WHERE e.dorsal=c2.dorsal);


-- consulta completa

-- para sacar nombre y edad 
-- realizo un join entre c3 y ciclista

SELECT 
    c.nombre, c.edad 
  FROM 
      ciclista c 
    JOIN
      (
        SELECT 
            c2.dorsal 
          FROM 
            (
              SELECT DISTINCT c.dorsal FROM ciclista c 
            ) c2
          WHERE 
            NOT EXISTS (SELECT 1 FROM etapa e WHERE e.dorsal=c2.dorsal)
      ) c3 USING(dorsal);


-- LA CONSULTA ENTERA SIN NECESIDAD DE RESTAR

SELECT 
    c.nombre,c.edad
  FROM 
    ciclista c
  WHERE 
    NOT EXISTS (SELECT 1 FROM etapa e WHERE e.dorsal=c.dorsal);


SELECT 
   c.nombre,c.edad
  FROM 
    ciclista c
  WHERE 
    c.dorsal NOT IN (SELECT DISTINCT dorsal FROM etapa);


SELECT 
    c.nombre,c.edad
  FROM 
      ciclista c
    LEFT JOIN
      (
        SELECT DISTINCT dorsal FROM etapa
      ) c1 USING(dorsal)
  WHERE 
    c1.dorsal IS NULL;


-- Ejercicio 3.- 	Listar el director de los equipos que tengan ciclistas 
-- que hayan ganado alguna etapa. 
-- (1) Sacar los ciclistas que han ganado etapas.
-- (2) Combinar el resultado con la tabla ciclistas y 
--     sacar los equipos que tienen ciclistas que han ganado etapas. 
-- (3) Combinar el resultado con la tabla que sea necesaria para sacar 
--     los directores que tengan ciclistas que han ganado etapas.

-- E1 (1)
SELECT DISTINCT dorsal  FROM etapa;

-- E2 (2)
SELECT 
    DISTINCT c.nomequipo 
  FROM 
      ciclista c 
    JOIN 
      (
        SELECT DISTINCT dorsal  FROM etapa
      ) E1  USING(dorsal);

-- (3)

-- CON JOIN

SELECT 
    DISTINCT e.director 
  FROM 
      equipo e
    JOIN
      (
        SELECT 
            DISTINCT c.nomequipo 
          FROM 
              ciclista c 
            JOIN 
              (
                SELECT DISTINCT dorsal  FROM etapa
              ) E1  USING(dorsal)      
     ) e2 USING(nomequipo);


-- CON IN 

SELECT 
    DISTINCT e.director
  FROM 
    equipo e
  WHERE 
    e.nomequipo IN 
      ( 
        SELECT 
          DISTINCT c.nomequipo 
        FROM 
            ciclista c 
          JOIN 
            (
              SELECT DISTINCT dorsal  FROM etapa
            ) E1  USING(dorsal)
      );

-- con un semijoin (exists)

SELECT 
    e.director 
  FROM 
    equipo e
  WHERE 
    EXISTS 
      ( 
       SELECT 
          1 
        FROM 
           (
            SELECT 
                DISTINCT c.nomequipo 
              FROM 
                  ciclista c 
                JOIN 
                  (
                    SELECT DISTINCT dorsal  FROM etapa
                  ) E1  USING(dorsal)
           ) e2
        WHERE e2.nomequipo=e.nomequipo
      );

-- para mañana



-- Ejercicio 4.- 	Dorsal y nombre de los ciclistas que hayan llevado algún maillot
-- (1) Sacar primero los dorsales de los ciclistas que han llevado algún maillot. 
-- (2) Combinar el resultado con la tabla ciclistas

-- c1 
SELECT DISTINCT dorsal FROM lleva l;

-- consulta
  SELECT 
    c.dorsal,
    c.nombre
    FROM (SELECT DISTINCT dorsal FROM lleva l) c1 JOIN ciclista c USING(dorsal);


-- Ejercicio 5.- 	Nombre y edad de los ciclistas que NO han ganado puertos 
-- Realizarlo utilizando el operador de resta de conjuntos. 
-- Como MYSQL no dispone de esta cláusula quiero que lo realicéis utilizando LEFT JOIN y después con NOT IN

-- c1: ciclistas que han ganado puertos
  SELECT DISTINCT dorsal FROM puerto;

-- implementamos la resta
-- consulta completa con LEFT JOIN
  SELECT c.nombre,c.edad 
  FROM ciclista c LEFT JOIN (SELECT DISTINCT dorsal FROM puerto) c1 USING(dorsal) 
  WHERE c1.dorsal IS NULL;


-- consulta completa con NOT IN
SELECT c.nombre,c.edad FROM ciclista c WHERE c.dorsal NOT IN (SELECT DISTINCT dorsal FROM puerto);


-- Ejercicio 6.- 	Listar el director de los equipos que ninguno de sus ciclistas haya ganado etapas
-- (1) Dorsal de los ciclistas que han ganado etapas. 
-- (2) Equipos que tienen ciclistas que han ganado etapas 
-- (3) Restar los equipos menos los equipos que han ganado etapas

  -- c1 : ciclistas que han ganado etapas
  SELECT DISTINCT e.dorsal FROM etapa e;

  -- c2: equipos que tienen ciclistas que han ganado etapas 
  SELECT DISTINCT c.nomequipo FROM ciclista c JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1 
    USING(dorsal);

  -- c3:restar 
    SELECT DISTINCT e.director 
      FROM equipo e 
      LEFT JOIN 
      (
        SELECT DISTINCT c.nomequipo FROM ciclista c JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1 
        USING(dorsal)
      ) c2
      USING(nomequipo)
      WHERE c2.nomequipo IS NULL;

-- Ejercicio 7.- 	Dorsal y nombre de los ciclistas que NO hayan llevado ningun maillot. 
-- Teneis que utilizar la resta

-- c1: dorsal de los ciclistas que han llevado maillot
  SELECT DISTINCT dorsal FROM lleva;

-- resta
  SELECT * 
    FROM ciclista c 
    LEFT JOIN (SELECT DISTINCT dorsal FROM lleva) c1
    ON c.dorsal=c1.dorsal
    WHERE c1.dorsal IS NULL;

-- Ejercicio 8.- 	Dorsal y nombre de los ciclistas que NO hayan llevado el maillot amarillo NUNCA. 
-- Tenéis que utilizar la resta

-- c1: codigo del maillot amarillo
  SELECT código FROM maillot WHERE color="amarillo" ;

-- c2: ciclistas que han llevado maillot amarillo
  SELECT DISTINCT l.dorsal 
    FROM lleva l JOIN (SELECT código FROM maillot WHERE color="amarillo") c1 
    USING(código);

-- consulta completa
-- realizamos la resta
SELECT c.dorsal,c.nombre 
  FROM ciclista c 
  LEFT JOIN 
  (
    SELECT DISTINCT l.dorsal 
      FROM lleva l JOIN (SELECT código FROM maillot WHERE color="amarillo") c1 
      USING(código)
  ) c2 USING(dorsal) 
  WHERE c2.dorsal IS NULL;


-- Ejercicio 9.- 	Listar el dorsal y el nombre de los ciclistas que hayan ganado alguna etapa que no tenga puerto
    
-- c1 : etapas que no tienen puerto
SELECT DISTINCT p.numetapa FROM puerto p;

-- c2 : ciclistas que han ganado etapas que no tienen puerto
-- resta
SELECT DISTINCT e.dorsal FROM etapa e LEFT JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1 
  USING(numetapa) WHERE c1.numetapa IS NULL;

-- final
SELECT c.dorsal,c.nombre 
  FROM ciclista c JOIN 
  (
    SELECT DISTINCT e.dorsal FROM etapa e LEFT JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1 
    USING(numetapa) WHERE c1.numetapa IS NULL
  ) c2  
  USING(dorsal);

-- Ejercicio 10.- 	Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos
-- (1) Listar las etapas con puertos.
-- (2) Listar los dorsales de los ciclistas que han ganado etapas con puertos. 
-- (3) Listar los dorsales de los ciclistas que han ganado etapas sin puertos. 
-- (4) Restar las consultas (3)-(2)

-- c1 : listar etapas con puertos
SELECT DISTINCT p.numetapa FROM puerto p;

-- c2 : ciclistas que han ganado etapas con puertos
SELECT DISTINCT e.dorsal FROM etapa e JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1 
  USING(numetapa);

-- c3
-- ciclistas que han ganado etapas que no tienen puertos
 SELECT DISTINCT e.dorsal FROM etapa e LEFT JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1 
  USING(numetapa) WHERE c1.numetapa IS NULL;

-- final
-- resto los ciclistas que han ganado etapas sin puertos menos los ciclistas que han ganado etapas con puertos

SELECT * FROM 
  (
    SELECT DISTINCT e.dorsal FROM etapa e LEFT JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1 
    USING(numetapa) WHERE c1.numetapa IS NULL
  ) c2
  LEFT JOIN 
  (
    SELECT DISTINCT e.dorsal FROM etapa e JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1 
    USING(numetapa)
  ) c1
  USING(dorsal)
  WHERE c1.dorsal IS NULL;
